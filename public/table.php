<?php
include_once "readingJSON.php";
include_once "functions/functions.php";


if(empty($_GET)){
	// echo "nema nisto";
}

else {
	
echo '<table class="table table-striped">';
echo '<thead>';
echo '<tr>';
echo  '<th scope="col"></th>';
echo  '<th scope="col">Id</th>';
echo  '<th scope="col">Order by rating</th>';
echo  '<th scope="col">Order by date</th>';
echo  '<th scope="col">Rating</th>';
echo '</tr>';
echo '</thead>';
echo '<tbody>';


if(isset($_GET['rating']) && $_GET['rating'] == '1') {
  	$arrayRating = filterByRating($someArray,'1');
  	foreach ($arrayRating as $result) {
	// code...
	echo '<tr>';
	echo '<th scope="row"></th>';
	echo '<td>'.$result['id'].'</td>';
	echo '<td>'.$result['reviewText'].'</td>';
	echo '<td>'.$result['reviewCreatedOnDate'].'</td>';
	echo '<td>'.$result['rating'].'</td>';
	echo '</tr>';
   }
}	

else if(isset($_GET['rating']) && $_GET['rating'] == '2') {
	$arrayRating = filterByRating($someArray,'2');
  	foreach ($arrayRating as $result) {
	// code...
	echo '<tr>';
	echo '<th scope="row"></th>';
	echo '<td>'.$result['id'].'</td>';
	echo '<td>'.$result['reviewText'].'</td>';
	echo '<td>'.$result['reviewCreatedOnDate'].'</td>';
	echo '<td>'.$result['rating'].'</td>';
	echo '</tr>';

   }
}

else if(isset($_GET['rating']) && $_GET['rating'] == '3') {

  	$arrayRating = filterByRating($someArray,'3');
  	foreach ($arrayRating as $result) {
	// code...
	echo '<tr>';
	echo '<th scope="row"></th>';
	echo '<td>'.$result['id'].'</td>';
	echo '<td>'.$result['reviewText'].'</td>';
	echo '<td>'.$result['reviewCreatedOnDate'].'</td>';
	echo '<td>'.$result['rating'].'</td>';
	echo '</tr>';

   }
}

else if(isset($_GET['rating']) && $_GET['rating'] == '4') {

  	$arrayRating = filterByRating($someArray,'4');
  	foreach ($arrayRating as $result) {
	// code...
	echo '<tr>';
	echo '<th scope="row"></th>';
	echo '<td>'.$result['id'].'</td>';
	echo '<td>'.$result['reviewText'].'</td>';
	echo '<td>'.$result['reviewCreatedOnDate'].'</td>';
	echo '<td>'.$result['rating'].'</td>';
	echo '</tr>';

   }
}

else if(isset($_GET['rating']) && $_GET['rating'] == '5') {

  	$arrayRating = filterByRating($someArray,'5');
  	foreach ($arrayRating as $result) {
	// code...
	echo '<tr>';
	echo '<th scope="row"></th>';
	echo '<td>'.$result['id'].'</td>';
	echo '<td>'.$result['reviewText'].'</td>';
	echo '<td>'.$result['reviewCreatedOnDate'].'</td>';
	echo '<td>'.$result['rating'].'</td>';
	echo '</tr>';

   }
}

else if(isset($_GET['prioritizeText']) && $_GET['prioritizeText'] == 'yes') {

	asort($someArray);
	foreach ($someArray as $result) {
	// code...
	echo '<tr>';
	echo '<th scope="row"></th>';
	echo '<td>'.$result['id'].'</td>';
	echo '<td>'.$result['reviewText'].'</td>';
	echo '<td>'.$result['reviewCreatedOnDate'].'</td>';
	echo '<td>'.$result['rating'].'</td>';
	echo '</tr>';

   }
}

else if(isset($_GET['orderId']) && $_GET['orderId'] == 'lowest') {

	asort($someArray);
	foreach ($someArray as $result) {
	// code...
	echo '<tr>';
	echo '<th scope="row"></th>';
	echo '<td>'.$result['id'].'</td>';
	echo '<td>'.$result['reviewText'].'</td>';
	echo '<td>'.$result['reviewCreatedOnDate'].'</td>';
	echo '<td>'.$result['rating'].'</td>';
	echo '</tr>';

   }
}

else {
  
  	foreach ($someArray as $result) {
	// code...
	echo '<tr>';
	echo '<th scope="row"></th>';
	echo '<td>'.$result['id'].'</td>';
	echo '<td>'.$result['reviewText'].'</td>';
	echo '<td>'.$result['reviewCreatedOnDate'].'</td>';
	echo '<td>'.$result['rating'].'</td>';
	echo '</tr>';

   }
  }
 }
?>
  </tbody>
</table>