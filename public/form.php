

<div class="container">
<h3 class="badge text-wrap" >Filter reviews</h3>



<form class="py-5" method="GET">

<label for="id" id="selectText">Order by Id: </label>
<select id="id" name="orderId" class="px-5 py-2 form-control">
  <option value="NoSelected">Choose an order</option>
  <option value="highest">Highest First</option>
  <option value="lowest">Lowest First</option>
</select>

<!-- <br> -->

<label for="rating" id="selectText">Order by Rating: </label>
<select id="rating" name="rating" class="px-5 py-2 form-control">
  <option value="NoSelected">Select rating..</option>
  <option value="1">1</option>
  <option value="2">2</option>
  <option value="3">3</option>
  <option value="4">4</option>
  <option value="5">5</option>
</select>

<!-- <br> -->

<label for="date" id="selectText">Order by date: </label>
<select id="date" name="date" class="px-5 py-2 form-control">
  <option value="NoSelected">Select filter by date..</option>
  <option value="oldest">Oldest First</option>
  <option value="newest">Newest First</option>
</select>

<!-- <br> -->

<label for="prioritizeText" id="selectText">Prioritize by text: </label>
<select id="prioritizeText" name="prioritizeText" class="px-5 py-2 form-control">
  <option value="NoSelected">Select prioritize text..</option>
  <option value="yes">Yes</option>
  <option value="no">No</option>
</select>


<input type="submit" class="btn btn-info" id="formButton" value="Filter">
</form>


</div>